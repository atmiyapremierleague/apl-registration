[![Build Status](https://api.travis-ci.org/umlet/umlet.svg?branch=master)](https://github.com/jatangohel/apl-registration)
# apl-registration
APL is a Multinational Sport Event organized by Yogi Divine Society, Montreal.
APL Registration is a a simple and dynamic user interface form developed with HTML5, CSS and JS incorporating GoogleMaps API's and other functionalities.

* Please check out the [Wiki] (https://github.com/jatangohel/apl-registration/wiki) for frequently asked questions

* Go to http://www.webmountstudio.com for the company details.
* Live Preview: http://www.webmountstudio.com/apl2019


## Development

- Take the code from the **develop** branch
- Create a Branch from the [Trello](https://trello.com/b/W61nJuAS/apl-development) task on the APL registration or whichever repository you are working on
- Raise a PR(Pull Request) based on the Bitbucket and add peer reviewers
- After one successful approval from the reviewer, the branch can then be merged on to Develop and this is how Develop will be updated with the latest code.

> **Note:** The **develop** is the main development branch here

